#!/usr/bin/env python3

import sys
import os
from random import choice

header = None


def check_args():
    if len(sys.argv) != 2:
        print("Usage: ./eng.py [test_name]", file=sys.stderr)
        sys.exit(1)


def parse_file():
    global header
    tests = []
    lines = open(sys.argv[1]).readlines()
    header = lines[0].split("|")[1:-1]
    lines = lines[2:]
    lines.reverse()
    while lines:
        line = lines.pop().split("|")[1:-1]
        test = {}
        for key, value in zip(header, line):
            test[key] = value
        tests.append(test)
    return tests


def intro(tests):
    os.system('clear')
    print("""
                E N G L I S H
    Written by Alexander Pateenok, 2017
    From 450501 with love,

         {number} words in {file}
    
    Press Enter to start (empty string to exit)
    """.format(number=len(tests), file=sys.argv[1]))
    input()
    os.system('clear')


def print_question(question, form, correct, all):
    print(f"ENG: {correct}/{all} right answers\n")
    quiz_form = choice(header[:-1]) if form == header[-1] else header[-1]
    print(f"""
    {quiz_form} : {question[quiz_form]}
    
    {form}?""")
    print()


def testing(tests):
    intro(tests)
    correct_answers = 0
    all_questions = 0
    errors = []
    answer = ''
    while tests:
        question = choice(tests)
        form = choice(header)
        print_question(question, form, correct_answers, all_questions)
        answer = input()
        if not answer:
            break
        if answer in question[form]:
            print("Correct!")
            tests.remove(question)
            correct_answers += 1
        else:
            errors.append(question)
            print("Nope")
        print_words([question])
        all_questions += 1
        input()
        os.system('clear')
    return errors


def print_words(errors):
    print("|".join(header))
    print("|".join(("-" * len(i)) for i in header))
    for err in errors:
        print("|".join(err.values()))

if __name__ == '__main__':
    check_args()
    errors = testing(parse_file())
    os.system("clear")
    print_words(errors)
